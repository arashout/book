# Download

Read this chapter for a detailed description on how to run Veloren and to avoid common pitfalls.

## Runtime dependencies

These are programs needed for Veloren to run on your device. Refer to [Compile](/compile/index.md) section for build-time dependencies.

**Currently there are no runtime dependencies**

## Releases

We provide pre-compiled versions of the game for Windows and Linux on <https://www.veloren.net/welcome>. You can run the Windows version on MacOS using [WINE](https://www.winehq.org/) or [compile a native version](/compile/index.md) yourself.

Download the version for your operating system and extract it, then open the `veloren-voxygen.exe` (Windows and WINE) or `veloren-voxygen` (Linux) file.

## Nightly

You can download latest developement builds from our [CI pipelines](https://gitlab.com/veloren/veloren/pipelines). You can launch them in the same way as releases.
